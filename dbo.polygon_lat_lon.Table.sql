USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[polygon_lat_lon]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[polygon_lat_lon](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[lat] [float] NOT NULL,
	[lng] [float] NOT NULL,
	[polygon] [numeric](19, 0) NULL,
 CONSTRAINT [UK_5s4ptnuqtd24d4p9au2rv53qc] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[polygon_lat_lon]  WITH CHECK ADD  CONSTRAINT [FK_sbdmgy1ru2m8bi1a06fgwltl3] FOREIGN KEY([polygon])
REFERENCES [dbo].[geo_polygon] ([id])
GO
ALTER TABLE [dbo].[polygon_lat_lon] CHECK CONSTRAINT [FK_sbdmgy1ru2m8bi1a06fgwltl3]
GO
