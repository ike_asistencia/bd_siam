USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[geo_polygon]    Script Date: 12/09/2019 01:00:04 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[geo_polygon](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[entity_id] [numeric](19, 0) NOT NULL,
	[type] [int] NOT NULL,
	[custom] [bit] NOT NULL,
	[appears] [varchar](16) NULL,
 CONSTRAINT [UK_5s4ptnuqtd24d4p9au2rv53qb] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_ct1dutj7e0l5mqye6hroa5jlq] UNIQUE NONCLUSTERED 
(
	[type] ASC,
	[entity_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[geo_polygon] ADD  CONSTRAINT [DF_geo_polygon_custom]  DEFAULT ('0') FOR [custom]
GO
