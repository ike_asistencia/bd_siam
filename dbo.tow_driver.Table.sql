USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[tow_driver]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tow_driver](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[active] [bit] NOT NULL,
	[busy] [bit] NOT NULL,
	[code] [varchar](32) NULL,
	[last_update] [datetime] NULL,
	[latitude] [float] NOT NULL,
	[longitude] [float] NOT NULL,
	[name] [varchar](128) NULL,
	[provider] [numeric](19, 0) NOT NULL,
	[app_version] [varchar](8) NULL,
	[device_id] [varchar](64) NULL,
	[assistance_type] [int] NULL,
 CONSTRAINT [PK__tow_driv__3213E83F3BD54180] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_9rwknfatkxejvkaq3a476cie6] UNIQUE NONCLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_jdnp5q1efryh1b5if1yjbi575] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[provider] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tow_driver] ADD  CONSTRAINT [DF_tow_driver_active]  DEFAULT ((0)) FOR [active]
GO
ALTER TABLE [dbo].[tow_driver] ADD  CONSTRAINT [DF_tow_driver_busy]  DEFAULT ((0)) FOR [busy]
GO
ALTER TABLE [dbo].[tow_driver] ADD  CONSTRAINT [DF_tow_driver_device_id]  DEFAULT ('0') FOR [device_id]
GO
ALTER TABLE [dbo].[tow_driver] ADD  CONSTRAINT [DF_tow_driver_assistance_type]  DEFAULT ('0') FOR [assistance_type]
GO
ALTER TABLE [dbo].[tow_driver]  WITH CHECK ADD  CONSTRAINT [FK_m9n5e3dp6b9waishqh79o2qkv] FOREIGN KEY([provider])
REFERENCES [dbo].[provider] ([id])
GO
ALTER TABLE [dbo].[tow_driver] CHECK CONSTRAINT [FK_m9n5e3dp6b9waishqh79o2qkv]
GO
