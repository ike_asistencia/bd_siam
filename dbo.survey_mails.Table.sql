USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[survey_mails]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[survey_mails](
	[id] [numeric](19, 0) NOT NULL,
	[record_number] [numeric](19, 0) NULL,
	[asunto] [varchar](255) NULL,
	[fecha_envio] [datetime] NULL,
	[destinatario] [varchar](255) NULL,
	[body] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
