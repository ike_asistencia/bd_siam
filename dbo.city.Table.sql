USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[city]    Script Date: 12/09/2019 01:00:04 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[city](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[code] [varchar](8) NOT NULL,
	[name] [varchar](128) NULL,
	[state] [varchar](8) NOT NULL,
 CONSTRAINT [PK__city__3213E83FB6D73E0B] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_6rkdh72pkw700w3ukrfbkte4f] UNIQUE NONCLUSTERED 
(
	[code] ASC,
	[state] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[city]  WITH CHECK ADD  CONSTRAINT [FK_1u3bjs31w5q4jqaredhp9kwkd] FOREIGN KEY([state])
REFERENCES [dbo].[state] ([code])
GO
ALTER TABLE [dbo].[city] CHECK CONSTRAINT [FK_1u3bjs31w5q4jqaredhp9kwkd]
GO
