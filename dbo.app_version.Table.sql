USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[app_version]    Script Date: 12/09/2019 01:00:04 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[app_version](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[message] [varchar](128) NOT NULL,
	[version_code] [int] NOT NULL,
	[version_name] [varchar](32) NOT NULL,
 CONSTRAINT [PK__app_v__3213E83F46358DE2] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
