USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[survey_answers]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[survey_answers](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[assistance] [numeric](19, 0) NULL,
	[survey] [numeric](19, 0) NULL,
 CONSTRAINT [PK__survey_a__3213E83F5D3A88AD] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[survey_answers]  WITH CHECK ADD  CONSTRAINT [FK_lma07e65q6ld7a6gcqw99dhr6] FOREIGN KEY([survey])
REFERENCES [dbo].[survey] ([id])
GO
ALTER TABLE [dbo].[survey_answers] CHECK CONSTRAINT [FK_lma07e65q6ld7a6gcqw99dhr6]
GO
ALTER TABLE [dbo].[survey_answers]  WITH CHECK ADD  CONSTRAINT [FK_pvxmk6si0cvdlre4qm7mbwj0h] FOREIGN KEY([assistance])
REFERENCES [dbo].[assistance] ([id])
GO
ALTER TABLE [dbo].[survey_answers] CHECK CONSTRAINT [FK_pvxmk6si0cvdlre4qm7mbwj0h]
GO
