USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[notification]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[notification](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[channel] [varchar](255) NOT NULL,
	[code] [varchar](255) NOT NULL,
	[current_status] [int] NOT NULL,
	[delay] [int] NOT NULL,
	[last_try] [datetime] NOT NULL,
	[message] [nvarchar](max) NULL,
	[notification_action] [int] NOT NULL,
	[notification_receiver] [int] NOT NULL,
	[received] [bit] NULL,
	[retry_counter] [int] NOT NULL,
	[assistance] [numeric](19, 0) NULL,
	[assistance_id] [numeric](19, 0) NOT NULL,
 CONSTRAINT [PK__notifica__3213E83FF9D29E43] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[notification] ADD  CONSTRAINT [DF_notification_received]  DEFAULT ((0)) FOR [received]
GO
ALTER TABLE [dbo].[notification] ADD  CONSTRAINT [DF_notification_assistance_id]  DEFAULT ('0') FOR [assistance_id]
GO
ALTER TABLE [dbo].[notification]  WITH CHECK ADD  CONSTRAINT [FK_a4p0dldpyymuqvouvq83b7xn2] FOREIGN KEY([assistance])
REFERENCES [dbo].[assistance] ([id])
GO
ALTER TABLE [dbo].[notification] CHECK CONSTRAINT [FK_a4p0dldpyymuqvouvq83b7xn2]
GO
