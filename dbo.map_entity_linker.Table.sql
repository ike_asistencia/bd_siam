USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[map_entity_linker]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[map_entity_linker](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[entity_type] [int] NOT NULL,
	[google_name] [varchar](128) NOT NULL,
	[sise_code] [varchar](16) NOT NULL,
	[sise_name] [varchar](128) NOT NULL,
 CONSTRAINT [PK__map_ent_li__6913883042015DEF] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_qq5okfj2ugy4gtw2b019xql9d] UNIQUE NONCLUSTERED 
(
	[google_name] ASC,
	[entity_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
