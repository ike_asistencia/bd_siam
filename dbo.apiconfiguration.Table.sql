USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[apiconfiguration]    Script Date: 12/09/2019 01:00:04 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[apiconfiguration](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[min_arrival_distance] [float] NOT NULL,
	[notification_max_retries] [int] NOT NULL,
	[notification_retry_delay] [int] NOT NULL,
	[provider_session_length] [int] NOT NULL,
	[search_driver_last_connection] [int] NOT NULL,
	[search_provider_interval] [int] NOT NULL,
	[search_radius] [float] NOT NULL,
	[search_providers_block_size] [int] NOT NULL,
	[configuration_lock] [numeric](19, 0) NOT NULL,
	[assign_assistance_timeout] [int] NOT NULL,
	[alert_speed_lower_limit] [float] NOT NULL,
	[eta_red_alert] [int] NOT NULL,
	[eta_update_interval] [int] NOT NULL,
	[alert_speed_interval] [int] NOT NULL,
	[truck_eta_factor] [float] NOT NULL,
	[foreign_city_name] [varchar](64) NULL,
	[foreign_state_name] [varchar](64) NULL,
	[search_time_limit] [float] NULL,
 CONSTRAINT [PK__apiconfi__3213E83F8F7BD7AB] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_75wk88u0fcv29p6tiatn6qxsw] UNIQUE NONCLUSTERED 
(
	[configuration_lock] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[apiconfiguration] ADD  CONSTRAINT [DF_apiconfiguration_min_arrival_distance]  DEFAULT ((300)) FOR [min_arrival_distance]
GO
ALTER TABLE [dbo].[apiconfiguration] ADD  CONSTRAINT [DF_apiconfiguration_notification_max_retries]  DEFAULT ((2)) FOR [notification_max_retries]
GO
ALTER TABLE [dbo].[apiconfiguration] ADD  CONSTRAINT [DF_apiconfiguration_notification_retry_delay]  DEFAULT ((60)) FOR [notification_retry_delay]
GO
ALTER TABLE [dbo].[apiconfiguration] ADD  CONSTRAINT [DF_apiconfiguration_provider_session_length]  DEFAULT ((1800)) FOR [provider_session_length]
GO
ALTER TABLE [dbo].[apiconfiguration] ADD  CONSTRAINT [DF_apiconfiguration_search_driver_last_connection]  DEFAULT ((120)) FOR [search_driver_last_connection]
GO
ALTER TABLE [dbo].[apiconfiguration] ADD  CONSTRAINT [DF_apiconfiguration_search_provider_interval]  DEFAULT ((60)) FOR [search_provider_interval]
GO
ALTER TABLE [dbo].[apiconfiguration] ADD  CONSTRAINT [DF_apiconfiguration_search_radius]  DEFAULT ((300)) FOR [search_radius]
GO
ALTER TABLE [dbo].[apiconfiguration] ADD  CONSTRAINT [DF_apiconfiguration_search_providers_block_size]  DEFAULT ((5)) FOR [search_providers_block_size]
GO
ALTER TABLE [dbo].[apiconfiguration] ADD  CONSTRAINT [DF_apiconfiguration_assign_assistance_timeout]  DEFAULT ((120)) FOR [assign_assistance_timeout]
GO
ALTER TABLE [dbo].[apiconfiguration] ADD  CONSTRAINT [DF_apiconfiguration_alert_speed_lower_limit]  DEFAULT ('10') FOR [alert_speed_lower_limit]
GO
ALTER TABLE [dbo].[apiconfiguration] ADD  CONSTRAINT [DF_apiconfiguration_eta_red_alert]  DEFAULT ('2700') FOR [eta_red_alert]
GO
ALTER TABLE [dbo].[apiconfiguration] ADD  CONSTRAINT [DF_apiconfiguration_eta_update_interval]  DEFAULT ('300') FOR [eta_update_interval]
GO
ALTER TABLE [dbo].[apiconfiguration] ADD  CONSTRAINT [DF_apiconfiguration_alert_speed_interval]  DEFAULT ('180') FOR [alert_speed_interval]
GO
ALTER TABLE [dbo].[apiconfiguration] ADD  CONSTRAINT [DF_apiconfiguration_truck_eta_factor]  DEFAULT ('1.0') FOR [truck_eta_factor]
GO
ALTER TABLE [dbo].[apiconfiguration] ADD  CONSTRAINT [DF_apiconfiguration_search_time_limit]  DEFAULT ((60)) FOR [search_time_limit]
GO
ALTER TABLE [dbo].[apiconfiguration]  WITH CHECK ADD  CONSTRAINT [FK_75wk88u0fcv29p6tiatn6qxsw] FOREIGN KEY([configuration_lock])
REFERENCES [dbo].[apiconfiguration_lock] ([id])
GO
ALTER TABLE [dbo].[apiconfiguration] CHECK CONSTRAINT [FK_75wk88u0fcv29p6tiatn6qxsw]
GO
