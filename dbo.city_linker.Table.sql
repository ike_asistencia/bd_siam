USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[city_linker]    Script Date: 12/09/2019 01:00:04 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[city_linker](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[city_name] [varchar](16) NULL,
	[google_name] [varchar](128) NOT NULL,
	[state_code] [varchar](16) NOT NULL,
 CONSTRAINT [PK__cit_li__0913A83F42018DE2] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_fyfscgy8rab4olvuvwu2vwbdn] UNIQUE NONCLUSTERED 
(
	[state_code] ASC,
	[google_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
