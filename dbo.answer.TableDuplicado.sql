USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[answer]    Script Date: 12/09/2019 01:00:04 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[answer](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[value] [varchar](255) NOT NULL,
	[question] [numeric](19, 0) NOT NULL,
	[survey_answers] [numeric](19, 0) NOT NULL,
 CONSTRAINT [PK__answer__3213E83FB13BFB7B] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[answer]  WITH CHECK ADD  CONSTRAINT [FK_auu1oex56jpgrjb5nyphg2hee] FOREIGN KEY([survey_answers])
REFERENCES [dbo].[survey_answers] ([id])
GO
ALTER TABLE [dbo].[answer] CHECK CONSTRAINT [FK_auu1oex56jpgrjb5nyphg2hee]
GO
ALTER TABLE [dbo].[answer]  WITH CHECK ADD  CONSTRAINT [FK_lc3dqr6kqx7ntw3kbf822ipwh] FOREIGN KEY([question])
REFERENCES [dbo].[question] ([id])
GO
ALTER TABLE [dbo].[answer] CHECK CONSTRAINT [FK_lc3dqr6kqx7ntw3kbf822ipwh]
GO
