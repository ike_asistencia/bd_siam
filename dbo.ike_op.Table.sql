USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[ike_op]    Script Date: 12/09/2019 01:00:04 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ike_op](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[active] [bit] NOT NULL,
	[name] [varchar](128) NULL,
	[sise_id] [numeric](19, 0) NULL,
 CONSTRAINT [PK__ike_op__3213E83F5DE555B4] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_kiww1esg0i84j7wiw65cwaiky] UNIQUE NONCLUSTERED 
(
	[sise_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ike_op] ADD  CONSTRAINT [DF_ike_op_active]  DEFAULT ((0)) FOR [active]
GO
