USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[state]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[state](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[code] [varchar](8) NOT NULL,
	[name] [varchar](64) NULL,
	[country] [varchar](8) NULL,
 CONSTRAINT [PK__state__3213E83F90D170A3] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_sleimnwe013aiopqeyp6qspg0] UNIQUE NONCLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[state]  WITH CHECK ADD  CONSTRAINT [FK_osnw35w0p3a1spsw4s8dqelv8] FOREIGN KEY([country])
REFERENCES [dbo].[country] ([code])
GO
ALTER TABLE [dbo].[state] CHECK CONSTRAINT [FK_osnw35w0p3a1spsw4s8dqelv8]
GO
