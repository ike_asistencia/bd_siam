USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[survey_question]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[survey_question](
	[survey] [numeric](19, 0) NOT NULL,
	[question] [numeric](19, 0) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[survey_question]  WITH CHECK ADD  CONSTRAINT [FK_9p4eudu7ubt2n9seacssoqykh] FOREIGN KEY([question])
REFERENCES [dbo].[question] ([id])
GO
ALTER TABLE [dbo].[survey_question] CHECK CONSTRAINT [FK_9p4eudu7ubt2n9seacssoqykh]
GO
ALTER TABLE [dbo].[survey_question]  WITH CHECK ADD  CONSTRAINT [FK_lpxc75t1p77yje9120wy5t6v8] FOREIGN KEY([survey])
REFERENCES [dbo].[survey] ([id])
GO
ALTER TABLE [dbo].[survey_question] CHECK CONSTRAINT [FK_lpxc75t1p77yje9120wy5t6v8]
GO
