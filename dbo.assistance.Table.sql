USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[assistance]    Script Date: 12/09/2019 01:00:04 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[assistance](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[canceled] [bit] NULL,
	[dest_city] [varchar](255) NULL,
	[dest_district] [varchar](255) NULL,
	[dest_latitude] [float] NULL,
	[dest_longitude] [float] NULL,
	[dest_notes] [varchar](255) NULL,
	[dest_state] [varchar](255) NULL,
	[dest_street] [varchar](255) NULL,
	[dest_zip_code] [varchar](255) NULL,
	[eta] [int] NULL,
	[eta_start] [datetime] NULL,
	[metadata] [nvarchar](max) NULL,
	[notes] [varchar](255) NULL,
	[city] [varchar](128) NULL,
	[district] [varchar](255) NULL,
	[latitude] [float] NULL,
	[longitude] [float] NULL,
	[origin_notes] [varchar](255) NULL,
	[state] [varchar](32) NULL,
	[street] [varchar](255) NULL,
	[zip_code] [varchar](8) NULL,
	[record_number] [varchar](64) NULL,
	[suggested_eta] [int] NULL,
	[verification_code] [varchar](64) NULL,
	[car] [numeric](19, 0) NULL,
	[current_status] [numeric](19, 0) NULL,
	[customer] [numeric](19, 0) NOT NULL,
	[customer_answers] [numeric](19, 0) NULL,
	[customer_survey] [numeric](19, 0) NOT NULL,
	[ike_op] [numeric](19, 0) NOT NULL,
	[inventory] [numeric](19, 0) NOT NULL,
	[inventory_answers] [numeric](19, 0) NULL,
	[provider_op] [numeric](19, 0) NULL,
	[tow_driver] [numeric](19, 0) NULL,
	[tow_truck] [numeric](19, 0) NULL,
	[need_inventory] [bit] NULL,
	[total_distance] [float] NULL,
	[last_eta_update] [datetime] NULL,
	[last_speed_alert] [datetime] NULL,
	[last_eta] [int] NULL,
	[eta_dest] [int] NULL,
	[distance_destination] [float] NULL,
	[distance_user] [float] NULL,
	[publicacionsiam] [bit] NULL,
	[providers_found] [int] NULL,
	[providers_list] [nvarchar](max) NULL,
	[type] [int] NULL,
 CONSTRAINT [PK__assistan__3213E83FC06AA5FB] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_kesjgkadhvf4qh3flrlocskwn] UNIQUE NONCLUSTERED 
(
	[record_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[assistance] ADD  CONSTRAINT [DF_assistance_canceled]  DEFAULT ((0)) FOR [canceled]
GO
ALTER TABLE [dbo].[assistance] ADD  CONSTRAINT [DF_assistance_eta]  DEFAULT ((0)) FOR [eta]
GO
ALTER TABLE [dbo].[assistance] ADD  CONSTRAINT [DF_assistance_latitude]  DEFAULT ((0)) FOR [latitude]
GO
ALTER TABLE [dbo].[assistance] ADD  CONSTRAINT [DF_assistance_longitude]  DEFAULT ((0)) FOR [longitude]
GO
ALTER TABLE [dbo].[assistance] ADD  CONSTRAINT [DF_assistance_suggested_eta]  DEFAULT ((0)) FOR [suggested_eta]
GO
ALTER TABLE [dbo].[assistance] ADD  CONSTRAINT [DF_assistance_need_inventory]  DEFAULT ((0)) FOR [need_inventory]
GO
ALTER TABLE [dbo].[assistance] ADD  CONSTRAINT [DF_assistance_total_distance]  DEFAULT ((0)) FOR [total_distance]
GO
ALTER TABLE [dbo].[assistance] ADD  CONSTRAINT [DF_assistance_last_eta]  DEFAULT ('0') FOR [last_eta]
GO
ALTER TABLE [dbo].[assistance] ADD  CONSTRAINT [DF_assistance_eta_dest]  DEFAULT ('0') FOR [eta_dest]
GO
ALTER TABLE [dbo].[assistance] ADD  CONSTRAINT [DF_assistance_distance_destination]  DEFAULT ((0)) FOR [distance_destination]
GO
ALTER TABLE [dbo].[assistance] ADD  CONSTRAINT [DF_assistance_distance_user]  DEFAULT ((0)) FOR [distance_user]
GO
ALTER TABLE [dbo].[assistance] ADD  CONSTRAINT [DF_assistance_publicacionsiam]  DEFAULT ('0') FOR [publicacionsiam]
GO
ALTER TABLE [dbo].[assistance] ADD  CONSTRAINT [DF_assistance_providers_found]  DEFAULT ('0') FOR [providers_found]
GO
ALTER TABLE [dbo].[assistance] ADD  CONSTRAINT [DF_assistance_type]  DEFAULT ('0') FOR [type]
GO
ALTER TABLE [dbo].[assistance]  WITH CHECK ADD  CONSTRAINT [FK_1cw4ofvq08yehwj4rn18c7xa3] FOREIGN KEY([inventory_answers])
REFERENCES [dbo].[survey_answers] ([id])
GO
ALTER TABLE [dbo].[assistance] CHECK CONSTRAINT [FK_1cw4ofvq08yehwj4rn18c7xa3]
GO
ALTER TABLE [dbo].[assistance]  WITH CHECK ADD  CONSTRAINT [FK_1v34orn2r769j2ui9edsqotl7] FOREIGN KEY([ike_op])
REFERENCES [dbo].[ike_op] ([id])
GO
ALTER TABLE [dbo].[assistance] CHECK CONSTRAINT [FK_1v34orn2r769j2ui9edsqotl7]
GO
ALTER TABLE [dbo].[assistance]  WITH CHECK ADD  CONSTRAINT [FK_35tpm1bucuiin4th66gj6vn3] FOREIGN KEY([customer_survey])
REFERENCES [dbo].[survey] ([id])
GO
ALTER TABLE [dbo].[assistance] CHECK CONSTRAINT [FK_35tpm1bucuiin4th66gj6vn3]
GO
ALTER TABLE [dbo].[assistance]  WITH CHECK ADD  CONSTRAINT [FK_6tscgy1qe72wuxk860ghotr8r] FOREIGN KEY([provider_op])
REFERENCES [dbo].[provider_op] ([id])
GO
ALTER TABLE [dbo].[assistance] CHECK CONSTRAINT [FK_6tscgy1qe72wuxk860ghotr8r]
GO
ALTER TABLE [dbo].[assistance]  WITH CHECK ADD  CONSTRAINT [FK_9g28pxsqkt041r1mva1yihy33] FOREIGN KEY([inventory])
REFERENCES [dbo].[survey] ([id])
GO
ALTER TABLE [dbo].[assistance] CHECK CONSTRAINT [FK_9g28pxsqkt041r1mva1yihy33]
GO
ALTER TABLE [dbo].[assistance]  WITH CHECK ADD  CONSTRAINT [FK_ac83cubs4blf4e535ibus7v59] FOREIGN KEY([tow_truck])
REFERENCES [dbo].[tow_truck] ([id])
GO
ALTER TABLE [dbo].[assistance] CHECK CONSTRAINT [FK_ac83cubs4blf4e535ibus7v59]
GO
ALTER TABLE [dbo].[assistance]  WITH CHECK ADD  CONSTRAINT [FK_ddyjf0is4daetkttv2bqb2ukg] FOREIGN KEY([customer])
REFERENCES [dbo].[customer] ([id])
GO
ALTER TABLE [dbo].[assistance] CHECK CONSTRAINT [FK_ddyjf0is4daetkttv2bqb2ukg]
GO
ALTER TABLE [dbo].[assistance]  WITH CHECK ADD  CONSTRAINT [FK_kpqt8sfrnrkimxljerurppvfa] FOREIGN KEY([customer_answers])
REFERENCES [dbo].[survey_answers] ([id])
GO
ALTER TABLE [dbo].[assistance] CHECK CONSTRAINT [FK_kpqt8sfrnrkimxljerurppvfa]
GO
ALTER TABLE [dbo].[assistance]  WITH CHECK ADD  CONSTRAINT [FK_mppt46a50yaqk24irhpuv0kt0] FOREIGN KEY([car])
REFERENCES [dbo].[car] ([id])
GO
ALTER TABLE [dbo].[assistance] CHECK CONSTRAINT [FK_mppt46a50yaqk24irhpuv0kt0]
GO
ALTER TABLE [dbo].[assistance]  WITH CHECK ADD  CONSTRAINT [FK_s1jxc5ygqcqy3cxm9fj3ie5f7] FOREIGN KEY([current_status])
REFERENCES [dbo].[status] ([id])
GO
ALTER TABLE [dbo].[assistance] CHECK CONSTRAINT [FK_s1jxc5ygqcqy3cxm9fj3ie5f7]
GO
ALTER TABLE [dbo].[assistance]  WITH CHECK ADD  CONSTRAINT [FK_tjcvs96p0bm10urjn33sis2ch] FOREIGN KEY([tow_driver])
REFERENCES [dbo].[tow_driver] ([id])
GO
ALTER TABLE [dbo].[assistance] CHECK CONSTRAINT [FK_tjcvs96p0bm10urjn33sis2ch]
GO
