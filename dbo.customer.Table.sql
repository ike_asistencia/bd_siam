USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[customer]    Script Date: 12/09/2019 01:00:04 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[customer](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[email] [varchar](255) NULL,
	[name] [varchar](128) NULL,
	[phone] [varchar](16) NULL,
 CONSTRAINT [PK__customer__3213E83F3552C4BB] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_he3o03uoi0k9prons7j5im8q] UNIQUE NONCLUSTERED 
(
	[email] ASC,
	[name] ASC,
	[phone] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
