USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[DATABASECHANGELOG]    Script Date: 12/09/2019 01:00:04 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DATABASECHANGELOG](
	[ID] [varchar](255) NOT NULL,
	[AUTHOR] [varchar](255) NOT NULL,
	[FILENAME] [varchar](255) NOT NULL,
	[DATEEXECUTED] [datetime] NOT NULL,
	[ORDEREXECUTED] [int] NOT NULL,
	[EXECTYPE] [varchar](10) NOT NULL,
	[MD5SUM] [varchar](35) NULL,
	[DESCRIPTION] [varchar](255) NULL,
	[COMMENTS] [varchar](255) NULL,
	[TAG] [varchar](255) NULL,
	[LIQUIBASE] [varchar](20) NULL
) ON [PRIMARY]
GO
