USE [APP_SIAM_COL]
GO
/****** Object:  StoredProcedure [dbo].[DB_ReindexNvo]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[DB_ReindexNvo] 
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @objectid int;
	DECLARE @indexid varchar(50);
	DECLARE @partitioncount bigint;
	DECLARE @schemaname nvarchar(130);
	DECLARE @objectname nvarchar(130);
	DECLARE @indexname nvarchar(130);
	DECLARE @partitionnum bigint;
	DECLARE @partitions bigint;
	DECLARE @frag float;
	DECLARE @command nvarchar(4000);
	DECLARE @dia INT;
	declare @Finicio datetime;
	select @Finicio =getdate(),
			@dia= datepart(dd,getdate()); 
	SELECT  Tabla= a.object_id,Indice=name, partition_number AS partitionnum,avg_fragmentation_in_percent  INTO #work_to_do
	FROM sys.dm_db_index_physical_stats(DB_ID(), NULL, NULL , NULL, 'LIMITED') AS a
		JOIN sys.indexes AS b ON a.object_id = b.object_id AND a.index_id = b.index_id
	where  avg_fragmentation_in_percent > 10.0 AND a.index_id > 0 and page_count>99 and allow_page_locks=1;
	
	DECLARE Reindexar cursor  FOR
		SELECT te.*,dia
		FROM #work_to_do te left join DB_Tablas_Reindex ta  on te.Tabla =object_id(ta.tabla) and ta.indice=te.indice 
		where ta.indice is null
		union all
		SELECT te.*,dia
		FROM #work_to_do te inner join DB_Tablas_Reindex ta  on te.Tabla =object_id(ta.tabla) and ta.indice=te.indice and ta.dia=3
		order by  dia desc ,avg_fragmentation_in_percent desc
		       
	OPEN Reindexar      
	WHILE (1=1) 
	BEGIN    
		--print 'entra'
		FETCH NEXT
           FROM Reindexar
           INTO @objectid, @indexname, @partitionnum, @frag,@dia;
        IF @@FETCH_STATUS < 0  or datediff (mi,@Finicio,getdate()) > 25 BREAK;
			SELECT @objectname = QUOTENAME(o.name), @schemaname = QUOTENAME(s.name)
			FROM sys.objects AS o	
				JOIN sys.schemas as s ON s.schema_id = o.schema_id
			WHERE o.object_id = @objectid;

        SELECT @partitioncount = count (*)
        FROM sys.partitions
        WHERE object_id = @objectid AND index_id = @indexid;
        IF @frag < 30.0
            SET @command = N'ALTER INDEX [' + @indexname + N'] ON ' + @schemaname + N'.' + @objectname + N' REORGANIZE';
        IF @frag >= 30.0
            SET @command = N'ALTER INDEX [' + @indexname + N'] ON ' + @schemaname + N'.' + @objectname + N' REORGANIZE; ALTER INDEX [' + @indexname + N'] ON ' + @schemaname + N'.' + @objectname + N' REBUILD';
        IF @partitioncount > 1
            SET @command = @command + N' PARTITION=' + CAST(@partitionnum AS nvarchar(10));
			PRINT N'Executed: ' + @command;
        EXEC (@command);
        PRINT N'Executed: ' + @command;
    END;
	CLOSE Reindexar     
	DEALLOCATE  Reindexar  
	-- Drop the temporary table.
	DROP TABLE #work_to_do;		
END;
GO