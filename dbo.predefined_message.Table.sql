USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[predefined_message]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[predefined_message](
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[message] [varchar](255) NOT NULL,
 CONSTRAINT [PK__predefin__3213E83F68AED162] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_fc3o3g03ss5sq66a2bw7r77p8] UNIQUE NONCLUSTERED 
(
	[message] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
