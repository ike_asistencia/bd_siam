USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[api_consumer]    Script Date: 12/09/2019 01:00:04 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[api_consumer](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[consumer_key] [varchar](255) NOT NULL,
	[name] [varchar](255) NOT NULL,
	[secret] [varchar](255) NOT NULL,
 CONSTRAINT [PK__api_cons__3213E83F4F9D0923] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_bvybdwsd1yq304d2gxsab2elt] UNIQUE NONCLUSTERED 
(
	[secret] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_fgvxe7018xpyjhr4owlc783x8] UNIQUE NONCLUSTERED 
(
	[consumer_key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_lp0up4p34xohvjotab47956ld] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
