USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[assistance_monitor]    Script Date: 12/09/2019 01:00:04 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[assistance_monitor](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[assistance] [numeric](19, 0) NULL,
	[continue_monitor] [int] NULL,
	[next_monitor] [datetime] NULL,
	[start_assistance] [datetime] NULL,
	[tow_driver] [numeric](19, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
