USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[tow_truck]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tow_truck](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[busy] [bit] NOT NULL,
	[plate] [varchar](16) NULL,
	[sise_id] [int] NULL,
	[provider] [numeric](19, 0) NOT NULL,
	[tow_truck_type] [int] NULL,
	[economic_number] [varchar](16) NULL,
	[tow_driver] [numeric](19, 0) NULL,
 CONSTRAINT [PK__tow_truc__3213E83F5E6736D6] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_cvokgkqqk62yuaalwj2n4kxcm] UNIQUE NONCLUSTERED 
(
	[sise_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_g3grmpocut3m2smk81yy196ma] UNIQUE NONCLUSTERED 
(
	[plate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tow_truck] ADD  CONSTRAINT [DF_tow_truck_busy]  DEFAULT ((0)) FOR [busy]
GO
ALTER TABLE [dbo].[tow_truck]  WITH CHECK ADD  CONSTRAINT [FK_34091ebdjrf6b3qw483n2kcoo] FOREIGN KEY([tow_driver])
REFERENCES [dbo].[tow_driver] ([id])
GO
ALTER TABLE [dbo].[tow_truck] CHECK CONSTRAINT [FK_34091ebdjrf6b3qw483n2kcoo]
GO
ALTER TABLE [dbo].[tow_truck]  WITH CHECK ADD  CONSTRAINT [FK_q8nk0j7qaqx93mrbhrgyj99xu] FOREIGN KEY([provider])
REFERENCES [dbo].[provider] ([id])
GO
ALTER TABLE [dbo].[tow_truck] CHECK CONSTRAINT [FK_q8nk0j7qaqx93mrbhrgyj99xu]
GO
