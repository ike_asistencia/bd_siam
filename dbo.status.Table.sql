USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[status]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[status](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[description] [varchar](255) NULL,
	[source] [int] NULL,
	[status] [int] NULL,
	[assistance] [numeric](19, 0) NOT NULL,
 CONSTRAINT [PK__status__3213E83F46358CE1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[status]  WITH CHECK ADD  CONSTRAINT [FK_6v0p0q51flq9esy4s63r8q8f6] FOREIGN KEY([assistance])
REFERENCES [dbo].[assistance] ([id])
GO
ALTER TABLE [dbo].[status] CHECK CONSTRAINT [FK_6v0p0q51flq9esy4s63r8q8f6]
GO
