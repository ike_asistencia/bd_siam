USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[chat_message]    Script Date: 12/09/2019 01:00:04 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[chat_message](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[message] [varchar](128) NULL,
	[sender] [int] NULL,
	[assistance] [numeric](19, 0) NOT NULL,
 CONSTRAINT [PK__chat_mes__3213E83FCADAC7F4] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[chat_message]  WITH CHECK ADD  CONSTRAINT [FK_78gjhtp19tyt8mtv1077cdifr] FOREIGN KEY([assistance])
REFERENCES [dbo].[assistance] ([id])
GO
ALTER TABLE [dbo].[chat_message] CHECK CONSTRAINT [FK_78gjhtp19tyt8mtv1077cdifr]
GO
