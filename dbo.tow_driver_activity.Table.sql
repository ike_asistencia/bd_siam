USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[tow_driver_activity]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tow_driver_activity](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[status] [int] NULL,
	[tow_driver] [numeric](19, 0) NULL,
	[elapsed_time] [float] NULL,
 CONSTRAINT [PK__tow_d_a__3213E83F46358CE2] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tow_driver_activity] ADD  CONSTRAINT [DF_tow_driver_activity_elapsed_time]  DEFAULT ((0)) FOR [elapsed_time]
GO
ALTER TABLE [dbo].[tow_driver_activity]  WITH CHECK ADD  CONSTRAINT [FK_pcfohwji1xmacpf76iw5i6tvw] FOREIGN KEY([tow_driver])
REFERENCES [dbo].[tow_driver] ([id])
GO
ALTER TABLE [dbo].[tow_driver_activity] CHECK CONSTRAINT [FK_pcfohwji1xmacpf76iw5i6tvw]
GO
