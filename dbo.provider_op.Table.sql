USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[provider_op]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[provider_op](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[active] [bit] NOT NULL,
	[code] [varchar](32) NOT NULL,
	[last_access] [datetime] NULL,
	[name] [varchar](128) NULL,
	[token] [varchar](32) NULL,
	[provider] [numeric](19, 0) NOT NULL,
 CONSTRAINT [PK__provider__3213E83F058776A3] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_pkmlkiawn2fa5a90omwxue8cm] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[provider] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_qucffrjln45o4v5hqohyv64wk] UNIQUE NONCLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_t9mjbeqr877wo6olqmmppab4p] UNIQUE NONCLUSTERED 
(
	[token] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[provider_op] ADD  CONSTRAINT [DF_provider_op_active]  DEFAULT ((0)) FOR [active]
GO
ALTER TABLE [dbo].[provider_op]  WITH CHECK ADD  CONSTRAINT [FK_dad7x2xdtse75s9v8egli1omm] FOREIGN KEY([provider])
REFERENCES [dbo].[provider] ([id])
GO
ALTER TABLE [dbo].[provider_op] CHECK CONSTRAINT [FK_dad7x2xdtse75s9v8egli1omm]
GO
