USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[search_configuration]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[search_configuration](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[block_size] [int] NOT NULL,
	[interval_next_seconds] [int] NOT NULL,
	[max_round_supported] [int] NOT NULL,
	[max_search_result] [int] NOT NULL,
	[name] [varchar](16) NOT NULL,
	[next_configuration] [varchar](16) NOT NULL,
	[radius_metres] [float] NOT NULL,
	[read_only] [bit] NOT NULL,
	[time_limit_minutes] [int] NOT NULL,
	[timeout_seconds] [int] NOT NULL,
	[speed_factor] [float] NOT NULL,
	[send_to] [int] NOT NULL,
	[has_priority] [bit] NULL,
 CONSTRAINT [PK__search_configuration__288CE83CE0A15496] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[search_configuration] ADD  CONSTRAINT [DF_search_configuration_block_size]  DEFAULT ((3)) FOR [block_size]
GO
ALTER TABLE [dbo].[search_configuration] ADD  CONSTRAINT [DF_search_configuration_interval_next_seconds]  DEFAULT ((60)) FOR [interval_next_seconds]
GO
ALTER TABLE [dbo].[search_configuration] ADD  CONSTRAINT [DF_search_configuration_max_round_supported]  DEFAULT ((1)) FOR [max_round_supported]
GO
ALTER TABLE [dbo].[search_configuration] ADD  CONSTRAINT [DF_search_configuration_max_search_result]  DEFAULT ((30)) FOR [max_search_result]
GO
ALTER TABLE [dbo].[search_configuration] ADD  CONSTRAINT [DF_search_configuration_radius_metres]  DEFAULT ((10000)) FOR [radius_metres]
GO
ALTER TABLE [dbo].[search_configuration] ADD  CONSTRAINT [DF_search_configuration_read_only]  DEFAULT ((0)) FOR [read_only]
GO
ALTER TABLE [dbo].[search_configuration] ADD  CONSTRAINT [DF_search_configuration_time_limit_minutes]  DEFAULT ((5)) FOR [time_limit_minutes]
GO
ALTER TABLE [dbo].[search_configuration] ADD  CONSTRAINT [DF_search_configuration_timeout_seconds]  DEFAULT ((60)) FOR [timeout_seconds]
GO
ALTER TABLE [dbo].[search_configuration] ADD  CONSTRAINT [DF_search_configuration_speed_factor]  DEFAULT ('1.0') FOR [speed_factor]
GO
ALTER TABLE [dbo].[search_configuration] ADD  CONSTRAINT [DF_search_configuration_send_to]  DEFAULT ('0') FOR [send_to]
GO
