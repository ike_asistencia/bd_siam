USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[map_configuration]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[map_configuration](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[bottom_lat] [float] NULL,
	[bottom_lng] [float] NULL,
	[center_lat] [float] NULL,
	[center_lng] [float] NULL,
	[country] [varchar](64) NOT NULL,
	[top_lat] [float] NULL,
	[top_lng] [float] NULL,
 CONSTRAINT [PK__map_configuration__2873E83CE0EE5496] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[map_configuration] ADD  CONSTRAINT [DF_map_configuration_bottom_lat]  DEFAULT ((0)) FOR [bottom_lat]
GO
ALTER TABLE [dbo].[map_configuration] ADD  CONSTRAINT [DF_map_configuration_bottom_lng]  DEFAULT ((0)) FOR [bottom_lng]
GO
ALTER TABLE [dbo].[map_configuration] ADD  CONSTRAINT [DF_map_configuration_center_lat]  DEFAULT ((0)) FOR [center_lat]
GO
ALTER TABLE [dbo].[map_configuration] ADD  CONSTRAINT [DF_map_configuration_center_lng]  DEFAULT ((0)) FOR [center_lng]
GO
ALTER TABLE [dbo].[map_configuration] ADD  CONSTRAINT [DF_map_configuration_top_lat]  DEFAULT ((0)) FOR [top_lat]
GO
ALTER TABLE [dbo].[map_configuration] ADD  CONSTRAINT [DF_map_configuration_top_lng]  DEFAULT ((0)) FOR [top_lng]
GO
