USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[DB_Tablas_Reindex]    Script Date: 12/09/2019 01:00:04 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_Tablas_Reindex](
	[clTablaReindex] [int] IDENTITY(1,1) NOT NULL,
	[Tabla] [varchar](500) NULL,
	[Indice] [varchar](500) NULL,
	[Dia] [int] NULL
) ON [PRIMARY]
GO
