USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[inventory_image]    Script Date: 12/09/2019 01:00:04 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[inventory_image](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[image_type] [int] NULL,
	[url] [varchar](255) NULL,
	[assistance] [numeric](19, 0) NOT NULL,
 CONSTRAINT [PK__inventor__3213E83F26923595] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[inventory_image]  WITH CHECK ADD  CONSTRAINT [FK_3r00n2oi65ujyhavnke4scihj] FOREIGN KEY([assistance])
REFERENCES [dbo].[assistance] ([id])
GO
ALTER TABLE [dbo].[inventory_image] CHECK CONSTRAINT [FK_3r00n2oi65ujyhavnke4scihj]
GO
