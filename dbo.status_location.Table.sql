USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[status_location]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[status_location](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[location] [numeric](19, 0) NOT NULL,
	[status] [numeric](19, 0) NOT NULL,
 CONSTRAINT [FK_d5odtslya2j795837l4hd6j9d] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[status_location]  WITH CHECK ADD  CONSTRAINT [FK_dfodt96ya2j7953l7l55d657g] FOREIGN KEY([location])
REFERENCES [dbo].[location] ([id])
GO
ALTER TABLE [dbo].[status_location] CHECK CONSTRAINT [FK_dfodt96ya2j7953l7l55d657g]
GO
ALTER TABLE [dbo].[status_location]  WITH CHECK ADD  CONSTRAINT [FK_f9kn0qcb3ffe24t0tn7ft7vp3] FOREIGN KEY([status])
REFERENCES [dbo].[status] ([id])
GO
ALTER TABLE [dbo].[status_location] CHECK CONSTRAINT [FK_f9kn0qcb3ffe24t0tn7ft7vp3]
GO
