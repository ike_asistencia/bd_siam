USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[hibernate_sequences]    Script Date: 12/09/2019 01:00:04 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[hibernate_sequences](
	[sequence_name] [varchar](255) NULL,
	[sequence_next_hi_value] [int] NULL
) ON [PRIMARY]
GO
