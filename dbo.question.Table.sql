USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[question]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[question](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[answers] [nvarchar](max) NULL,
	[detail] [varchar](255) NULL,
	[question] [varchar](255) NULL,
	[value_type] [int] NULL,
 CONSTRAINT [PK__question__3213E83FF0EE5416] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
