USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[tow_driver_monitor]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tow_driver_monitor](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[assistance_id] [numeric](19, 0) NULL,
	[code_sise_response] [varchar](255) NULL,
	[error_sise_response] [varchar](255) NULL,
	[monitor_status] [int] NULL,
	[record_number] [varchar](255) NULL,
	[status_sise_response] [varchar](255) NULL,
	[tow_driver_id] [numeric](19, 0) NULL,
	[transaction_sise_response] [varchar](255) NULL,
	[tow_driver_date] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
