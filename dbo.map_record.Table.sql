USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[map_record]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[map_record](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[lat_destination] [float] NULL,
	[lat_origin] [float] NULL,
	[lng_destination] [float] NULL,
	[lng_origin] [float] NULL,
	[record_number] [varchar](64) NOT NULL,
 CONSTRAINT [PRIMARY] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_tpypxs2vytnyty6e1c86m811v] UNIQUE NONCLUSTERED 
(
	[record_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
