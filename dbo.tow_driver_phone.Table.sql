USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[tow_driver_phone]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tow_driver_phone](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[assistance] [numeric](19, 0) NULL,
	[phone_number] [varchar](255) NULL,
	[record_number] [varchar](255) NULL,
	[tow_driver] [numeric](19, 0) NULL,
	[code_sise_response] [varchar](255) NULL,
	[error_sise_response] [varchar](255) NULL,
	[provider_id] [numeric](19, 0) NULL,
	[status_sise_response] [varchar](255) NULL,
	[transaction_sise_response] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
