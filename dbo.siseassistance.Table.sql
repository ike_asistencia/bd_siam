USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[siseassistance]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[siseassistance](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[city] [varchar](255) NULL,
	[dest_city] [varchar](255) NULL,
	[dest_latitude] [float] NULL,
	[dest_longitude] [float] NULL,
	[dest_state] [varchar](255) NULL,
	[latitude] [float] NULL,
	[longitude] [float] NULL,
	[record_number] [varchar](64) NULL,
	[state] [varchar](255) NULL,
	[status] [int] NULL,
	[last_update] [datetime] NOT NULL,
	[assigment_timestamp] [datetime] NULL,
 CONSTRAINT [PK__sise_assis__6913883042015DEF] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[siseassistance] ADD  CONSTRAINT [DF_siseassistance_dest_latitude]  DEFAULT ((0.0)) FOR [dest_latitude]
GO
ALTER TABLE [dbo].[siseassistance] ADD  CONSTRAINT [DF_siseassistance_dest_longitude]  DEFAULT ((0.0)) FOR [dest_longitude]
GO
ALTER TABLE [dbo].[siseassistance] ADD  CONSTRAINT [DF_siseassistance_latitude]  DEFAULT ((0.0)) FOR [latitude]
GO
ALTER TABLE [dbo].[siseassistance] ADD  CONSTRAINT [DF_siseassistance_longitude]  DEFAULT ((0.0)) FOR [longitude]
GO
ALTER TABLE [dbo].[siseassistance] ADD  CONSTRAINT [DF_siseassistance_status]  DEFAULT ((0)) FOR [status]
GO
ALTER TABLE [dbo].[siseassistance] ADD  CONSTRAINT [DF_siseassistance_last_update]  DEFAULT (getdate()) FOR [last_update]
GO
