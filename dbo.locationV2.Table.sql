USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[locationV2]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[locationV2](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[latitude] [float] NOT NULL,
	[longitude] [float] NOT NULL,
	[assistance] [numeric](19, 0) NOT NULL,
 CONSTRAINT [PK__location2] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[locationV2]  WITH CHECK ADD  CONSTRAINT [FK_location2_assistance] FOREIGN KEY([assistance])
REFERENCES [dbo].[assistance] ([id])
GO
ALTER TABLE [dbo].[locationV2] CHECK CONSTRAINT [FK_location2_assistance]
GO
