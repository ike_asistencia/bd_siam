USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[assistance_discarded_driver]    Script Date: 12/09/2019 01:00:04 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[assistance_discarded_driver](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[assistance_id] [numeric](19, 0) NOT NULL,
	[discarded_list] [nvarchar](max) NULL,
 CONSTRAINT [PK__assistance_discarded_driver__288CE83CE0A15474] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[assistance_discarded_driver] ADD  CONSTRAINT [DF_assistance_discarded_driver_assistance_id]  DEFAULT ((0)) FOR [assistance_id]
GO
