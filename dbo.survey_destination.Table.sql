USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[survey_destination]    Script Date: 12/09/2019 01:00:05 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[survey_destination](
	[id] [numeric](5, 0) NOT NULL,
	[created] [datetime] NULL,
	[email] [varchar](255) NULL,
	[active] [varchar](5) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
