USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[car]    Script Date: 12/09/2019 01:00:04 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[car](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[brand] [varchar](32) NOT NULL,
	[model] [varchar](32) NOT NULL,
	[plate] [varchar](16) NOT NULL,
	[year] [int] NOT NULL,
 CONSTRAINT [PK__car__3213E83FC0A26230] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_hl5ws0mfeqyileabu6mkrfjtm] UNIQUE NONCLUSTERED 
(
	[plate] ASC,
	[brand] ASC,
	[model] ASC,
	[year] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
