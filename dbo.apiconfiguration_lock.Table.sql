USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[apiconfiguration_lock]    Script Date: 12/09/2019 01:00:04 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[apiconfiguration_lock](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[configuration] [numeric](19, 0) NULL,
 CONSTRAINT [PK__apiconfi__3213E83FED031109] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_3qovgvl2w01y40n51l6t1g6cn] UNIQUE NONCLUSTERED 
(
	[configuration] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[apiconfiguration_lock]  WITH CHECK ADD  CONSTRAINT [FK_3qovgvl2w01y40n51l6t1g6cn] FOREIGN KEY([configuration])
REFERENCES [dbo].[apiconfiguration] ([id])
GO
ALTER TABLE [dbo].[apiconfiguration_lock] CHECK CONSTRAINT [FK_3qovgvl2w01y40n51l6t1g6cn]
GO
