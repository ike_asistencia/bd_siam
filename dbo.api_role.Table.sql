USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[api_role]    Script Date: 12/09/2019 01:00:04 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[api_role](
	[id] [numeric](19, 0) NOT NULL,
	[created] [datetime] NOT NULL,
	[name] [varchar](255) NOT NULL,
	[role] [varchar](255) NOT NULL,
 CONSTRAINT [PK__api_role__3213E83F9D0455F9] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
