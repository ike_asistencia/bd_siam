USE [APP_SIAM_COL]
GO
/****** Object:  Table [dbo].[api_consumer_role]    Script Date: 12/09/2019 01:00:04 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[api_consumer_role](
	[consumer] [numeric](19, 0) NOT NULL,
	[role] [numeric](19, 0) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[api_consumer_role]  WITH CHECK ADD  CONSTRAINT [FK_7it9yg38ivntpoprks4tcqxhg] FOREIGN KEY([role])
REFERENCES [dbo].[api_role] ([id])
GO
ALTER TABLE [dbo].[api_consumer_role] CHECK CONSTRAINT [FK_7it9yg38ivntpoprks4tcqxhg]
GO
ALTER TABLE [dbo].[api_consumer_role]  WITH CHECK ADD  CONSTRAINT [FK_rp2cat6gqiopehnoc14k1i5af] FOREIGN KEY([consumer])
REFERENCES [dbo].[api_consumer] ([id])
GO
ALTER TABLE [dbo].[api_consumer_role] CHECK CONSTRAINT [FK_rp2cat6gqiopehnoc14k1i5af]
GO
